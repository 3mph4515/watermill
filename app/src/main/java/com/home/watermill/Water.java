/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.home.watermill;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;

/**
 * A two-dimensional square for use as a drawn object in OpenGL ES 1.0/1.1.
 */
public class Water {

    private final FloatBuffer vertexBuffer;

    // number of coordinates per vertex in this array
    private float vertices[] = {
            // BACK
            0.0f, 0.0f, 3.5f,  // 0. left-bottom-front
            25.0f, 0.0f, 3.5f,  // 1. right-bottom-front
            0.0f, 4.9f, 3.5f,  // 2. left-top-front
            25.0f, 4.9f, 3.5f,  // 3. right-top-front
            // FRONT
            25.0f, 0.0f, 0.0f,  // 6. right-bottom-back
            0.0f, 0.0f, 0.0f,  // 4. left-bottom-back
            25.0f, 4.9f, 0.0f,  // 7. right-top-back
            0.0f, 4.9f, 0.0f,  // 5. left-top-back
            // LEFT
            0.0f, 0.0f, 0.0f,  // 4. left-bottom-back
            0.0f, 0.0f, 3.5f,  // 0. left-bottom-front
            0.0f, 4.9f, 0.0f,  // 5. left-top-back
            0.0f, 4.9f, 3.5f,  // 2. left-top-front
            // RIGHT
            25.0f, 0.0f, 3.5f,  // 1. right-bottom-front
            25.0f, 0.0f, 0.0f,  // 6. right-bottom-back
            25.0f, 4.9f, 3.5f,  // 3. right-top-front
            25.0f, 4.9f, 0.0f,  // 7. right-top-back
            // TOP
            0.0f, 4.9f, 3.5f,  // 2. left-top-front
            25.0f, 4.9f, 3.5f,  // 3. right-top-front
            0.0f, 4.9f, 0.0f,  // 5. left-top-back
            25.0f, 4.9f, 0.0f,  // 7. right-top-back
            // BOTTOM
            0.0f, 0.0f, 0.0f,  // 4. left-bottom-back
            25.0f, 0.0f, 0.0f,  // 6. right-bottom-back
            0.0f, 0.0f, 3.5f,  // 0. left-bottom-front
            25.0f, 0.0f, 3.5f   // 1. right-bottom-front
    };

    float color[] = {0.0f, 0.0f, 1.0f, 0.5f};

    /**
     * Sets up the drawing object data for use in an OpenGL ES context.
     */
    public Water() {
        // a float has 4 bytes so we allocate for each coordinate 4 bytes
        ByteBuffer vertexByteBuffer = ByteBuffer.allocateDirect(vertices.length * 4);
        vertexByteBuffer.order(ByteOrder.nativeOrder());

        // allocates the memory from the byte buffer
        vertexBuffer = vertexByteBuffer.asFloatBuffer();

        // fill the vertexBuffer with the vertices
        vertexBuffer.put(vertices);

        // set the cursor position to the beginning of the buffer
        vertexBuffer.position(0);
    }

    public void draw(GL10 gl) {
        gl.glFrontFace(GL10.GL_CCW);    // Front face in counter-clockwise orientation
        gl.glEnable(GL10.GL_CULL_FACE); // Enable cull face
        gl.glCullFace(GL10.GL_BACK);    // Cull the back face (don't display)

        gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
        gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBuffer);

        // Render all the faces
        for (int face = 0; face < 6; face++) {
            // Set the color for each of the faces
            gl.glColor4f(color[0], color[1], color[2], color[3]);
            // Draw the primitive from the vertex-array directly
            gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, face * 4, 4);
        }
        gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
        gl.glDisable(GL10.GL_CULL_FACE);
    }
}