package com.home.watermill;

import android.opengl.GLSurfaceView;
import android.opengl.GLU;

import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.Random;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class GlRenderer implements GLSurfaceView.Renderer {


    private static float LIGHT_X = 5f;
    private static float LIGHT_Y = 3f;
    private static float LIGHT_Z = -5.4f;

    private final Land mLandLeft;
    private final Roof mRoof;
    private final BuildingLittle mBuildingLittle;
    private final Rail mRail;
    private Rectangle mRectangle;


    private Water mWater;
    private Flow mFlow;
    private Building mBuilding;

    private static float anglePyramid = 0;
    private static float angleCube = 0;
    private static float speedPyramid = 1.0f;
    private static float speedCube = -1.5f;

    private ArrayList<FlowObject> mFlowObjects;
    private float mX, mY;
    private Land mLandRight;

    private final static float lightAmb[]= { 0.5f, 0.5f, 0.5f, 0.5f };
    private final static float lightDif[]= { 1f, 1f, 1f, 1.0f };
    private final static float lightPos[]= { LIGHT_X, LIGHT_Y, LIGHT_Z, 1.0f };

    private final static FloatBuffer lightAmbBfr;
    private final static FloatBuffer lightDifBfr;
    private final static FloatBuffer lightPosBfr;

    static {
        lightAmbBfr = FloatBuffer.wrap(lightAmb);
        lightDifBfr = FloatBuffer.wrap(lightDif);
        lightPosBfr = FloatBuffer.wrap(lightPos);
    }

    private BuildingLittle mLight;

    /**
     * Constructor
     */
    public GlRenderer() {
        this.mRectangle = new Rectangle();
        this.mWater = new Water();
        this.mFlow = new Flow();
        this.mBuilding = new Building();
        this.mBuildingLittle = new BuildingLittle();
        this.mLight = new BuildingLittle();
        this.mLandRight = new Land();
        this.mLandLeft = new Land();
        this.mRoof = new Roof();
        this.mRail = new Rail();
        this.mFlowObjects = new ArrayList<FlowObject>();
        Random rand = new Random();
        for (int i = 0; i < 500; i++) {
            float z = rand.nextFloat() * (-9f  + 5.5f) - 5.5f;
            mFlowObjects.add(new FlowObject(-12.7f, -1.3f, z, new Flow()));
        }

    }

    @Override
    public void onDrawFrame(GL10 gl) {
        //Clear Screen And Depth Buffer
        gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
        gl.glEnable(GL10.GL_BLEND);
        gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
        gl.glMatrixMode(GL10.GL_MODELVIEW);
        gl.glLoadIdentity();                    //Reset The Current Modelview Matrix


        GLU.gluLookAt(gl, 1.0f, mY, mX, 0.0f, 0.1f, 0.0f, 0.0f, 0.1f, 0.0f);


        gl.glPushMatrix();
        gl.glTranslatef(LIGHT_X, LIGHT_Y, LIGHT_Z);
        mLight.draw(gl);
        gl.glPopMatrix();

        gl.glPushMatrix();
        gl.glTranslatef(-2.0f, -1.2f, -12.0f);
        mBuilding.draw(gl);
        gl.glPopMatrix();

        gl.glPushMatrix();
        gl.glTranslatef(-0.5f, -1.2f, -5.4f);
        mBuildingLittle.draw(gl);
        gl.glPopMatrix();

        gl.glPushMatrix();
        gl.glTranslatef(-2.0f, 1.8f, -12.0f);
        mRoof.draw(gl);
        gl.glPopMatrix();

        gl.glPushMatrix();
        gl.glTranslatef(-0.025f, -0.8f, -9.25f);
        mRail.draw(gl);
        gl.glPopMatrix();

        for (int i = 0; i < 360; i = i + 45) {
            gl.glPushMatrix();
            gl.glTranslatef(0.0f, -0.8f, -6.5f);
            gl.glRotatef(90f, -1, 0, 0);
            gl.glRotatef(i - anglePyramid, 0, 1, 0);
            mRectangle.draw(gl);
            gl.glPopMatrix();
        }

        gl.glPushMatrix();
        gl.glTranslatef(-12.7f, -6.1f, -9f);
        mWater.draw(gl);
        gl.glPopMatrix();

        gl.glPushMatrix();
        gl.glTranslatef(-12.7f, -6.1f, -34.0f);
        mLandRight.draw(gl);
        gl.glPopMatrix();

        gl.glPushMatrix();
        gl.glTranslatef(-12.7f, -6.1f, -5.5f);
        mLandLeft.draw(gl);
        gl.glPopMatrix();



        for (FlowObject fo : mFlowObjects) {
            createNewFlow(gl, fo);
            fo.setNowX(fo.getNowX() + fo.getSpeed());
        }


        anglePyramid += speedPyramid;
        angleCube += speedCube;
    }

    public void createNewFlow(GL10 gl, FlowObject fo) {
        gl.glPushMatrix();
        gl.glTranslatef(fo.getNowX(), fo.getNowY(), fo.getNowZ());
        fo.getmFlow().draw(gl);
        gl.glPopMatrix();
        if (fo.getNowX() > 12.3f) {
            fo.setNowX(fo.getStartX());
        }
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        if (height == 0) {                        //Prevent A Divide By Zero By
            height = 1;                        //Making Height Equal One
        }

        gl.glViewport(0, 0, width, height);    //Reset The Current Viewport
        gl.glMatrixMode(GL10.GL_PROJECTION);    //Select The Projection Matrix
        gl.glLoadIdentity();                    //Reset The Projection Matrix

        //Calculate The Aspect Ratio Of The Window
        GLU.gluPerspective(gl, 45.0f, (float) width / (float) height, 0.1f, 100.0f);

        gl.glMatrixMode(GL10.GL_MODELVIEW);    //Select The Modelview Matrix
        gl.glLoadIdentity();                    //Reset The Modelview Matrix
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        gl.glClearDepthf(1.0f);

        gl.glEnable(GL10.GL_DEPTH_TEST);
        gl.glDepthFunc(GL10.GL_LEQUAL);
        gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT, GL10.GL_NICEST);
        gl.glCullFace(GL10.GL_BACK);
        gl.glShadeModel(GL10.GL_SMOOTH);/*
        gl.glDisable(GL10.GL_DITHER);
        gl.glEnable(GL10.GL_TEXTURE_2D);*/

        gl.glEnable(GL10.GL_LIGHT0);
        gl.glEnable(GL10.GL_COLOR_MATERIAL);
        gl.glLightfv(GL10.GL_LIGHT0, GL10.GL_AMBIENT, lightAmbBfr);
        gl.glLightfv(GL10.GL_LIGHT0, GL10.GL_DIFFUSE, lightDifBfr);
        gl.glLightfv(GL10.GL_LIGHT0, GL10.GL_POSITION, lightPosBfr);
    }

    public float getX() {
        return mX;
    }

    public void setX(float x) {
        mX = x;
    }

    public float getY() {
        return mY;
    }

    public void setY(float y) {
        mY = y;
    }
}
