package com.home.watermill;

import android.util.Log;

import java.util.Random;

/**
 * Created by Flower Power on 06.10.2014.
 */
public class FlowObject {

    float minX = 0.01f;
    float maxX = 0.05f;

    private float mStartX;
    private float mStartY;

    private float mNowX;
    private float mNowY;
    private float mNowZ;

    public Flow getmFlow() {
        return mFlow;
    }

    public void setmFlow(Flow mFlow) {
        this.mFlow = mFlow;
    }

    private Flow mFlow;

    private float mSpeed;

    public FlowObject(float startX, float startY, float startZ, Flow mFlow)

    {
        mStartX = startX;
        mStartY = startY;
        mNowX = startX;
        mNowY = startY;
        mNowZ = startZ;
        this.mFlow = mFlow;

        Random rand = new Random();
        mSpeed = rand.nextFloat() * (maxX - minX) + minX;
        Log.d("Created", "New flow object created with speed: " + mSpeed);
    }


    public float getStartX() {
        return mStartX;
    }

    public void setStartX(float startX) {
        mStartX = startX;
    }

    public float getStartY() {
        return mStartY;
    }

    public void setStartY(float startY) {
        mStartY = startY;
    }

    public float getNowX() {
        return mNowX;
    }

    public void setNowX(float nowX) {
        mNowX = nowX;
    }

    public float getNowY() {
        return mNowY;
    }

    public void setNowY(float nowY) {
        mNowY = nowY;
    }

    public float getSpeed() {
        return mSpeed;
    }

    public void setSpeed(float speed) {
        mSpeed = speed;
    }

    public float getNowZ() {
        return mNowZ;
    }

    public void setNowZ(float nowZ) {
        mNowZ = nowZ;
    }
}
